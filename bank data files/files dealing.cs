﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;


namespace bank_data_files
{
    class files_dealing : IonFolderChange
    {
        public void onFolderChange(string folderPath){
            filesToFibi(folderPath);
        }

        public void filesToFibi(string scannedFolderPath){
            int i=0;
            string[] lines = new string[30];

            string todaysDate=DateTime.Now.ToString("ddMMyy");
            string filefName = Path.GetFileName(scannedFolderPath);
            string watchedBank = Path.GetFileName(Path.GetDirectoryName(scannedFolderPath));
            string destDirectory = @"Q:\H_sal\Weber\fibi files tester\";
            //string destDirectory = @"\\hhw-rgdnap04\danel-rlf\KLITOT\TEUDOT SAL\sal benleumi\From_Harel\";
            string BankDataConfigPath= @"Q:\H_sal\Weber\bank conf files\" ;
            string bankFileName=scannedToChecked(watchedBank);

            string dailyBankConfFile=BankDataConfigPath + "History\\" + todaysDate + "\\" + bankFileName;
            
            if(!Directory.Exists( @"Q:\H_sal\Weber\bank conf files\History\" + todaysDate ))
                Directory.CreateDirectory( @"Q:\H_sal\Weber\bank conf files\History\" + todaysDate );

            if (System.IO.File.Exists(dailyBankConfFile))
            {
                var list = new List<string>();
                using (StreamReader reader = new StreamReader(dailyBankConfFile))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                        list.Add(line);
                    lines = list.ToArray();
                }
                //lines = System.IO.File.ReadAllLines(dailyBankConfFile);
            }
            else{
                var list = new List<string>();
                using (StreamReader reader = new StreamReader(BankDataConfigPath + bankFileName))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                        list.Add(line);
                    lines = list.ToArray();
                }
                //lines = System.IO.File.ReadAllLines(BankDataConfigPath + bankFileName);
                var endDataFile = System.IO.File.Create(BankDataConfigPath + "History\\" + todaysDate + "\\" + bankFileName);
                endDataFile.Close();
            }
            

            string[] arrFiles = new string[lines.Length];
            string[] arrStatus = new string[lines.Length];
            string[] arrBank = new string[lines.Length];
            string[] arrLog = new string[lines.Length];
            string[] arrNewData = new string[lines.Length];
            string[] arrBaseData = new string[lines.Length];
            Boolean allGood = true;
            DateTime[] arrDates = new DateTime[lines.Length];
            
            foreach (string line in lines){
                arrBank[i]=line.Split(new string[] { "---" },StringSplitOptions.None)[0];
                arrFiles[i] = line.Split(new string[] { "---" }, StringSplitOptions.None)[1];
                arrDates[i] = DateTime.Parse(line.Split(new string[] { "---" }, StringSplitOptions.None)[2]);
                arrStatus[i] = line.Split(new string[] { "---" }, StringSplitOptions.None)[3];
                if (arrStatus[i].Equals("Not yet") || string.IsNullOrEmpty(arrStatus[i]))
                    allGood = false;
                i++;
            }
            if (allGood)
                return;
            string tempFileName = null;
            DateTime relevancyDate = new DateTime();
            for (int j = 0; j < i ; j++){
                tempFileName = arrFiles[j];
                if (!arrStatus[j].Equals("Good")){
                    //check if file exists
                    if (!System.IO.File.Exists(tempFileName))
                    {
                        bool isExists = false;
                        var dir = new DirectoryInfo(Path.GetDirectoryName(tempFileName));
                        var gg = Path.GetFileName(tempFileName);
                        FileInfo[] files = dir.GetFiles(Path.GetFileName(tempFileName) + "*", SearchOption.TopDirectoryOnly);
                        foreach (var file in files){
                            isExists = true;
                            tempFileName = file.FullName;
                            break;
                        }
                        if (!isExists){
                            arrLog[j] = "file is missing";
                            arrStatus[j] = "Not yet";
                            goto nextJ;
                        }
                    }
                    //check if file is not empty
                    if (new FileInfo(tempFileName).Length == 0)
                    {
                        arrLog[j] = "file is Empty";
                        arrStatus[j] = "Not yet";
                        goto nextJ;
                    }
                    relevancyDate = getRelevancyDate(tempFileName);
                    if (relevancyDate.Equals(DateTime.MinValue)){
                        goto nextJ;
                    }
                    if (relevancyDate.Date < arrDates[j].Date)
                    {
                        arrLog[j] = "file is not updated";
                        arrStatus[j] = "Not yet";    
                    }
                    else{
                        arrLog[j] = null;
                        arrStatus[j] = "Good";
                        arrDates[j] = relevancyDate.Date;
                        System.IO.File.Copy(tempFileName, destDirectory + bankFileName + "\\" + System.IO.Path.GetFileName(tempFileName), true);
                    }
                }
            nextJ:
                arrNewData[j] = arrBank[j] + "---" + arrFiles[j] + "---" + arrDates[j].ToShortDateString() + "---" + arrStatus[j] + "---" + arrLog[j];
                arrBaseData[j] = arrBank[j] + "---" + arrFiles[j] + "---" + arrDates[j].ToShortDateString() + "------" ;  

            }
            using (StreamWriter writer = new StreamWriter(dailyBankConfFile)){
                foreach (string line in arrNewData)
                    writer.WriteLine(line);
            }
            using (StreamWriter writer2 = new StreamWriter(BankDataConfigPath + bankFileName)){
                foreach (string line in arrBaseData)
                    writer2.WriteLine(line);
            }                        
        }


        private DateTime getRelevancyDate(string bankDataFile){

            string all=null;
            Thread.Sleep(1000);
            using (FileStream fs = new FileStream(bankDataFile, FileMode.Open, FileAccess.Read)){
                using (StreamReader sr = new StreamReader(fs)) 
                    all = sr.ReadToEnd();
            }
            string[] lines=all.Split('\n');
            System.Globalization.CultureInfo enUS = new System.Globalization.CultureInfo("en-US");
            DateTime paramFromDate;
            DateTime dt = new DateTime();
            string str = null;
            bool goodDate = true;
            bool notRelevantFile = false;
            string fName = System.IO.Path.GetFileName(bankDataFile);
            //DISKONT
            if (fName.Contains("MNTNYRS") || fName.Contains("MNTNYRSD")) str = lines[0].Substring(2, 6) + ";ddMMyy";
            else if (fName.Contains("LAK")) str = lines[0].Substring(134, 6) + ";ddMMyy";
            else if (fName.Contains("YIT")) str = lines[0].Substring(75, 6) + ";ddMMyy";
            else if (fName.Contains("PRTKRNMN")) str = lines[0].Substring(10, 6) + ";ddMMyy";
            else if (fName.Contains("HashDanel")) str = lines[0].Substring(42, 8) + ";ddMMyyyy";
            else if (fName.Contains("YHM")) str = lines[0].Substring(40, 6) + ";ddMMyy";
            else if (fName.Contains("DTAK")) str = lines[0].Substring(47, 6) + ";ddMMyy";
            else if (fName.Contains("OSH")) str = lines[0].Substring(39, 6) + ";yyMMdd";
            else if (fName.Contains("MDDBURS")) str = lines[0].Substring(8, 6) + ";yyMMdd";
            else if (fName.Contains("TNN")) str = lines[0].Substring(14, 6) + ";yyMMdd";
            else if (fName.Contains("TSN")) str = lines[0].Substring(32, 6) + ";yyMMdd";
            else if (fName.Contains("HOZIM")) str = lines[1].Substring(316, 6) + ";yyMMdd";
            else if (fName.Contains("DHUPLUS")) str = lines[0].Substring(26, 6) + ";yyMMdd";
            else if (fName.Contains("PIRT152")) str = lines[0].Substring(8, 6) + ";yyMMdd";
            //LEUMI
            else if (fName.Contains("HARELSAL_LEUMI_DANEL")) str = lines[0].Substring(0, 8) + ";yyyyMMdd";
            else if (fName.Contains("HARELSAL_FW_NR_DANEL")) str = lines[0].Substring(67, 8) + ";yyyyMMdd";
            else if (fName.Contains("HARELSAL_LEUMI_HZ")) str = lines[0].Substring(0, 8) + ";yyyyMMdd";
            //IGUD
            else if (fName.Contains("HARELSAL_IGUD_DANEL")) str = lines[0].Substring(0, 8) + ";yyyyMMdd";
            else if (fName.Contains("HAREL_PIKDONOT_")) str = fName.Substring(15, 6) + ";ddMMyy";
            //MIZI
            else if (fName.Contains("HARELS.MPK")) str = lines[0].Substring(0, 8) + ";yyyyMMdd";
            else if (fName.Contains("HARELS.LEG")) str = lines[1].Substring(4, 8) + ";yyyyMMdd";
            else if (fName.Contains("CY80E")) str = fName.Substring(5, 6) + ";yyMMdd";
            else if (fName.Contains("HARELS.DSK"))
            {
                foreach (string line in lines)
                {
                    if (! string.IsNullOrEmpty(line))
                    {
                        if (line.Substring(16, 2).Equals("00")){
                            str = line.Substring(83, 6) + ";yyMMdd";
                            break;
                        }
                    }
                }
            }
            //IBI
            else if (fName.Contains("DN8") && fName.Contains(".106")) str = lines[0].Substring(0, 8) + ";yyyyMMdd";
            //XNES
            else if (fName.Contains("DN8") && fName.Contains(".128")) str = lines[0].Substring(0, 8) + ";yyyyMMdd";
            //MTDS
            else if (fName.Contains("DN8") && fName.Contains(".132")) str = lines[0].Substring(0, 8) + ";yyyyMMdd";
            //NADA
            else notRelevantFile = true;
            if (!notRelevantFile)
            {
                string[] dtData = str.Split(';');
                goodDate = DateTime.TryParseExact(dtData[0], dtData[1], enUS, System.Globalization.DateTimeStyles.None, out paramFromDate);
                if (goodDate) dt = paramFromDate;
            }
            else
                dt = DateTime.MinValue;
            return dt;
        }


        private string scannedToChecked(string scannedFolder) {
            string bankInnerName=null;
            switch (scannedFolder) { 
                case "discount":
                    bankInnerName = "Discount";
                    break;
                case "sal ibi":
                    bankInnerName = "IBI";
                    break;
                case "IGUD":
                    bankInnerName = "Igud";
                    break;
                case "sal igud":
                    bankInnerName = "Igud";
                    break;
                case "SAL_MEITAV_DASH":
                    bankInnerName = "Mtds";
                    break;
                case "sal excellence":
                    bankInnerName = "Xnes";
                    break;
                case "k_mizrachi":
                    bankInnerName = "Mizrahi";
                    break;
                case "LEUMI":
                    bankInnerName = "Leumi";
                    break;
                }
                return bankInnerName;
            }
        

       }
 }
