﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace bank_data_files
{
    
    class folder_watcher
    {
        FileSystemWatcher watcher;
        IonFolderChange handler=new files_dealing();


        public void watch(string[] paths){
            foreach (string path in paths){
                if (System.IO.Directory.Exists(path)){
                    watcher = new FileSystemWatcher();
                    watcher.Path = path;
                    watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
                                           | NotifyFilters.FileName | NotifyFilters.DirectoryName;
                    watcher.Filter = "*.*";
                    watcher.Changed += new FileSystemEventHandler(OnChanged);
                    watcher.Created += new FileSystemEventHandler(OnCreation);
                    watcher.EnableRaisingEvents = true;
                }
            }
        }
        public void stopWatching(){
            watcher.EnableRaisingEvents = false;        
        }

        private void OnChanged(object source, FileSystemEventArgs e){
                onFolderChange(e.FullPath,handler);
        }
        private void onFolderChange(string path,IonFolderChange handler){
                handler.onFolderChange(path);       
        }
        private void OnCreation(object source, FileSystemEventArgs e){
            onFolderChange(e.FullPath, handler);
        }

    }
}
